// start build
module Build

#r "packages/FAKE/tools/FakeLib.dll"
#r "packages/Fantomas/lib/FantomasLib.dll"
#r "packages/Suave/lib/net40/Suave.dll"
#r "packages/FAKE/tools/FakeLib.dll"
#load "app.fsx"
open App
open Fake
open System
open System.IO
open Suave
open Suave.Http
open Suave.Web
open Fake.Paket
open System.Net

let serverConfig =
    let port = int (getBuildParam "port")
    { defaultConfig with
        homeFolder = Some __SOURCE_DIRECTORY__
        logger = Logging.Loggers.saneDefaultsFor Logging.LogLevel.Warn
        bindings = [ Suave.Http.HttpBinding.mk Suave.Http.HTTP (IPAddress.Parse "127.0.0.1") 8083us ] }

Target "Run" (fun _ ->
    startWebServer serverConfig app
)

RunTargetOrDefault "Run"
