#!/bin/bash
mono .paket/paket.bootstrapper.exe

mono .paket/paket.exe restore

[ ! -e build.fsx ] && mono .paket/paket.exe update
[ ! -e build.fsx ] && mono packages/FAKE/tools/FAKE.exe init.fsx
mono packages/FAKE/tools/FAKE.exe $@ --fsiargs -d:MONO build.fsx 