﻿#if BOOTSTRAP
System.Environment.CurrentDirectory <- __SOURCE_DIRECTORY__
if not (System.IO.File.Exists "paket.exe") then let url = "https://github.com/fsprojects/Paket/releases/download/2.40.9/paket.exe" in use wc = new System.Net.WebClient() in let tmp = System.IO.Path.GetTempFileName() in wc.DownloadFile(url, tmp); System.IO.File.Move(tmp,System.IO.Path.GetFileName url);;
#r "paket.exe"
Paket.Dependencies.Install (System.IO.File.ReadAllText "paket.dependencies")
#endif

//---------------------------------------------------------------------

#I "packages"
#I "packages/Chiron/lib/net40/"
#I "packages/FParsec/lib/net40-client/"
#r "packages/Suave/lib/net40/Suave.dll"
#r "FParsecCS.dll"
#r "FParsec.dll"
#r "packages/Aether/lib/net35/Aether.dll"
#r "Chiron.dll"
#load "./Types.fsx"

open Suave
open Suave.Http
open Suave.Operators
open Suave.Filters
open Suave.Successful
open Suave.Files
open Suave.RequestErrors
open Suave.Logging
open Suave.Utils
open Suave.Successful
open Suave.RequestErrors

open System
open System.Net

open Suave.Sockets
open Suave.Sockets.Control
open Suave.WebSocket

open Chiron
open Types

let jsonMime = Writers.setMimeType "application/json; charset=utf-8"
let setCORSHeaders =
    Writers.setHeader  "Access-Control-Allow-Origin" "*"
    >=> Writers.setHeader "Access-Control-Allow-Headers" "*"
    >=> Writers.setHeader "Access-Control-Allow-Methods" "GET, POST"


let mutable chatMembers : Registration List = []
let mutable messages : Messages = []

let parseMessage input : Choice<Message, string>=
    match Json.tryParse input with
    | Choice1Of2 success -> Choice1Of2 (Json.deserialize success)
    | Choice2Of2 error -> Choice2Of2 error

let addMessageToState input = 
  printfn "server received: %A" input
  match parseMessage input with
  | Choice1Of2 message -> 
    messages <- message::messages
    messages
  | Choice2Of2 error -> 
    printfn "parse error: %A" error
    messages

type BroadcastType = BroadcastText | BroadcastClose 
let broadCast = new Event<(byte[] * BroadcastType)>()
let broadCasted = broadCast.Publish

let echo (ws: WebSocket) ctx =
    let notifyLoop =
        async
            { 
              let loop = ref true
              while !loop do 
                let! (msg, broadcastType) = Async.AwaitEvent broadCasted
                try
                  let! a =  ws.send Text msg true
                  return ()
                with
                | _ -> 
                  loop := false
                  return ()
            }
    let cts = new System.Threading.CancellationTokenSource()
    Async.Start(notifyLoop, cts.Token)

    socket {
      let loop = ref true
      while !loop do
        let! m = ws.read()
        match m with
        | Text,data,true ->
            let ms = addMessageToState <| System.Text.Encoding.UTF8.GetString(data)
            let out = Json.format <| formatMessagesAndRegistrations ms chatMembers
            let bytes = System.Text.Encoding.UTF8.GetBytes(out)
            broadCast.Trigger(bytes, BroadcastText)             
        | Ping,_,_ ->   ()
        | Close,_,_ ->
          loop := false
          broadCast.Trigger([||], BroadcastClose)
        | _ -> ()
      }

let deserializeRegistration (r: Json) : Registration = 
    r |> Json.deserialize
    
let register registration =
    match registration with
    | (json,_)::_ ->
        let j = Json.tryParse(json)
        match j with
        | Choice1Of2 success ->
            let r = deserializeRegistration success
            chatMembers <- r::chatMembers
            let out = Json.format <| formatMessagesAndRegistrations messages chatMembers
            let bytes = System.Text.Encoding.UTF8.GetBytes(out)
            broadCast.Trigger(bytes, BroadcastText) 
            OK <| (Json.serialize r |> Json.formatWith JsonFormattingOptions.Pretty)
        | Choice2Of2 error -> 
            BAD_REQUEST error
    | _ -> BAD_REQUEST """expected json such as: { "name" : "joris" }"""

let app : WebPart =
  choose [
    path "/websocket" >=> handShake echo
    POST >=> choose [ path "/register" >=> request (fun r -> register r.form) >=> jsonMime >=> setCORSHeaders];
    GET >=> choose [ path "/users" >=> setCORSHeaders >=> jsonMime >=> request (fun _ -> chatMembers |> serializeListRegistrations |> Json.format |> OK) ]
    path "/" >=> Suave.Files.file "public/index.html"
    path "/app.js" >=> Suave.Files.file "public/app.js"
    NOT_FOUND "Found no handlers."
  ]


let config =
    let port = System.Environment.GetEnvironmentVariable("PORT")
    let ip127  = IPAddress.Parse("127.0.0.1")
    let ipZero = IPAddress.Parse("0.0.0.0")

    { defaultConfig with
        logger = Logging.Loggers.saneDefaultsFor Logging.LogLevel.Verbose
        bindings =
            [ (if port = null
                then HttpBinding.mk HTTP ip127 (uint16 8083)
                else HttpBinding.mk HTTP ipZero (uint16 port))
            ]
    }
    
startWebServer { config with logger = Loggers.ConsoleWindowLogger LogLevel.Verbose } app
