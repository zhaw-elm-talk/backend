module Types

#I "packages"
#r "packages/Aether/lib/net35/Aether.dll"
#r "packages/Chiron/lib/net40/Chiron.dll"

open Chiron
open Chiron.Operators
open System


type Message = 
    {
        message: string
        user: string
    }
    static member FromJson (_:Message) =
        (fun n u ->
            { message = n 
              user = u } )
        <!> Json.read "message"
        <*> Json.read "user"

    static member ToJson (x:Message) =
        Json.write "message" x.message
        *> Json.write "user" x.user
        

type Registration = 
    {
        name: string
        uuid: string
    }
    static member FromJson (_:Registration) =
        (fun n ->
            { name = n 
              uuid = Guid.NewGuid().ToString() } )
        <!> Json.read "name"

            
    static member ToJson (x:Registration) =
        Json.write "name" x.name
        *> Json.write "uuid" x.uuid

let serializeListRegistration (r : Registration) =
    [
        ("name", Chiron.String r.name)
    ]
    |> Map.ofList
    |> Chiron.Object

let serializeListRegistrations (rs : Registration list) =
    rs
    |> List.map serializeListRegistration
    |> Chiron.Array

type Messages = Message list

let serializeMessages (ms : Messages) =
  Json.Array <| List.map Json.serialize ms

type MessagesAndRegistrations =
    {
        messages : Messages
        registrations : Registration list
    }

let fMessagesAndRegistrations mrs =
    [
        ("messages", serializeMessages mrs.messages)
        ("registrations", serializeListRegistrations mrs.registrations)
    ]
    |> Map.ofList
    |> Chiron.Object

let uuidToName chatMembers (message : Message) : Message = 
  match List.tryFind (fun m -> m.uuid = message.user) chatMembers with
  | Some user -> 
    { message = message.message
      user = user.name
    }
  | None -> 
    { message = message.message
      user = "Not registered User"
    }

let uuidToNames chatMembers =
  List.map (uuidToName chatMembers)
  
let formatMessagesAndRegistrations messages registrations =
    {
        messages = uuidToNames registrations messages
        registrations = registrations
    }
    |> fMessagesAndRegistrations

let fromJsonFoldWith deserialize fold zero xs =
    let folder =
        fun r x ->
            match r with
            | Error e -> Error e
            | Value xs ->
                match deserialize x with
                | Value x -> Value (fold x xs)
                | Error e -> Error e
    List.fold folder (Value zero) (List.rev xs)

let listFromJsonWith deserialize =
    function
    | Array l -> fromJsonFoldWith deserialize (fun x xs -> x::xs) [] l
    | _ -> Error "Expected an array"
